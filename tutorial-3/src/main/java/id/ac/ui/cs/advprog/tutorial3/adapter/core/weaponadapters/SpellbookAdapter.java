package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean chargedAttackCooldown;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.chargedAttackCooldown = false;
    }

    @Override
    public String normalAttack() {
        chargedAttackCooldown = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!chargedAttackCooldown) {
            chargedAttackCooldown = true;
            return spellbook.largeSpell();
        } else {
            chargedAttackCooldown = false;
            return "Charge Attack on cooldown!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
