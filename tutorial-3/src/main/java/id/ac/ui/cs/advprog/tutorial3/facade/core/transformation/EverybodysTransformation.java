package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class EverybodysTransformation {
    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        if (encode) {
            spell = new CelestialTransformation().encode(spell);
            spell = new AbyssalTransformation().encode(spell);
            spell = new UwuTransformation().encode(spell);
            spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
            return spell;
        } else {
            spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
            spell = new UwuTransformation().decode(spell);
            spell = new AbyssalTransformation().decode(spell);
            spell = new CelestialTransformation().decode(spell);
            return spell;
        }
    }
}
