package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "senjata event";
    }

    @Override
    public String chargedAttack() {
        return "untung ngelarin eventnya jadi punya pedang limited";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
