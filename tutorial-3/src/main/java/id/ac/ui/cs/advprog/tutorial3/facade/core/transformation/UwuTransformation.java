package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
/**
 * Kelas ini mengimplementasikan sistem kriptografi caesar cipher
 */
public class UwuTransformation {
    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        int selector = encode ? 3 : -3;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);
            int newIdx = (charIdx + selector) % codex.getCharSize();
            newIdx = newIdx < 0 ? codex.getCharSize() + newIdx : newIdx;
            res[i] = codex.getChar(newIdx);
        }
        return new Spell(new String(res), codex);
    }
}
