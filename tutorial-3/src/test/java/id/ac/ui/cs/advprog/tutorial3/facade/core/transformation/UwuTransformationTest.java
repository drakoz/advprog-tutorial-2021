package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UwuTransformationTest {
    private Class<?> uwuClass;

    @BeforeEach
    public void setup() throws Exception {
        uwuClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.UwuTransformation");
    }

    @Test
    public void testUwuHasEncodeMethod() throws Exception {
        Method translate = uwuClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testUwuEncodesCorrectly() throws Exception {
        String text = "asd";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "dvg";

        Spell result = new UwuTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testUwuHasDecodeMethod() throws Exception {
        Method translate = uwuClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testUwuDecodesCorrectly() throws Exception {
        String text = "dvg";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "asd";

        Spell result = new UwuTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
