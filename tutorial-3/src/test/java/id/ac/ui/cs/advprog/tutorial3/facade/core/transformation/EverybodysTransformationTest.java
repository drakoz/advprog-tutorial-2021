package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EverybodysTransformationTest {
    private Class<?> everybodyClass;

    @BeforeEach
    public void setup() throws Exception {
        everybodyClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.EverybodysTransformation");
    }

    @Test
    public void testEverybodyHasEncodeMethod() throws Exception {
        Method translate = everybodyClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testEverybodyEncodesCorrectly() throws Exception {
        String text = "towasama maji daitenshi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "aGSD$$B{D@bVB+MBBFJGHH[";

        Spell result = new EverybodysTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testEverybodyHasDecodeMethod() throws Exception {
        Method translate = everybodyClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testEverybodyDecodesCorrectly() throws Exception {
        String text = "aGSD$$B{D@bVB+MBBFJGHH[";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "towasama maji daitenshi";

        Spell result = new EverybodysTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}

