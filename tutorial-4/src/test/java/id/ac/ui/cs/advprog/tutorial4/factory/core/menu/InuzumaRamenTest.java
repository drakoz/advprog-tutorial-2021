package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;
    private Menu inuzumaRamen;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Class<?> parentClass = inuzumaRamenClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testInuzumaRamenGetNameMethod() {
        String expected = "Inuzuma Ramen";
        assertEquals(inuzumaRamen.getName(), expected);
    }

    @Test
    public void testInuzumaRamenGetNoodle() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenGetMeat() {
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenGetTopping() {
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaRamenGetFlavor() {
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }
}
