package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderDrinkTest {
    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testOrderDrinkSetDrink() {
        assertEquals("", orderDrink.toString());
        orderDrink.setDrink("ngeteh enjoyer");
        assertEquals("ngeteh enjoyer", orderDrink.toString());
    }
}
