package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @Mock
    private List<Menu> list;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        list = new ArrayList<>();
    }

    @Test
    public void testMenuRepositoryGetMenuItShouldReturnMenuList() {
        ReflectionTestUtils.setField(menuRepository, "list", list);
        List<Menu> acquiredList = menuRepository.getMenus();
        assertThat(acquiredList).isEqualTo(list);
    }

    @Test
    public void testMenuRepositoryAddItShouldReturnMenu() {
        ReflectionTestUtils.setField(menuRepository, "list", list);
        Menu newMenu = new InuzumaRamen("ramen");
        Menu acquiredMenu = menuRepository.add(newMenu);

        assertThat(acquiredMenu).isEqualTo(newMenu);
    }
}
