package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    private Class<?> orderFoodClass;
    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderFoodClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testOrderFoodSetFood() {
        assertEquals("", orderFood.toString());
        orderFood.setFood("banana enjoyer");
        assertEquals("banana enjoyer", orderFood.toString());
    }
}