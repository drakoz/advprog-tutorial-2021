package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    private Menu liyuanSoba;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("Liyuan Soba");
    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenu() {
        Class<?> parentClass = liyuanSobaClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testLiyuanSobaGetNameMethod() {
        String expected = "Liyuan Soba";
        assertEquals(liyuanSoba.getName(), expected);
    }

    @Test
    public void testLiyuanSobaGetNoodle() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaGetMeat() {
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaGetTopping() {
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
    }

    @Test
    public void testLiyuanSobaGetFlavor() {
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }
}
