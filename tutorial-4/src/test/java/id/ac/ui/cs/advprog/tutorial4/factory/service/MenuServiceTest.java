package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {

    @Spy
    private MenuRepository menuRepository = new MenuRepository();

    @InjectMocks
    private MenuServiceImpl menuService = new MenuServiceImpl();

    @Test
    public void testMenuServiceGetMenusCorrectlyImplemented() {
        menuService.getMenus();
        verify(menuRepository, atLeastOnce()).getMenus();
    }

    @Test
    public void testMenuServiceCreateMenuCorrectlyImplemented() {
        menuService.createMenu("Gyudone", "MondoUdon");
        assertEquals(1, menuRepository.getMenus().size());
        verify(menuRepository, atLeastOnce()).add(any(MondoUdon.class));
    }

    @Test
    public void testMenuServiceGetMenusReturnMenus() {
        List<Menu> mockMenuList = new ArrayList<>();
        mockMenuList.add(new MondoUdon("Gyudon"));
        when(menuRepository.getMenus()).thenReturn(mockMenuList);
        assertEquals(1, menuService.getMenus().size());
        verify(menuRepository, atLeastOnce()).getMenus();
    }

}
