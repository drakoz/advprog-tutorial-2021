package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    @Mock
    private OrderDrink orderDrink;

    @Mock
    private OrderFood orderFood;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testOrderServiceOrderADrinkCorrectlyImplemented() {
        orderService.orderADrink("minuman");
        verify(orderDrink, atLeastOnce()).setDrink("minuman");
    }

    @Test
    public void testOrderServiceOrderAFoodCorrectlyImplemented() {
        orderService.orderAFood("makanan");
        verify(orderFood, atLeastOnce()).setFood("makanan");
    }
}
