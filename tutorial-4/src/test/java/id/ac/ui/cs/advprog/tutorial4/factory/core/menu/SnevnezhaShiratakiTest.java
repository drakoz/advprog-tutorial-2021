package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    private Menu snevnezhaShirataki;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("Namanya ribet");
    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenu() {
        Class<?> parentClass = snevnezhaShiratakiClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetNameMethod() {
        String expected = "Namanya ribet";
        assertEquals(snevnezhaShirataki.getName(), expected);
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodle() {
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiGetMeat() {
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiGetTopping() {
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavor() {
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }
}