package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    private Menu mondoUdon;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("Is Gyudon an Udon?");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Class<?> parentClass = mondoUdonClass.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu",
                parentClass.getName());
    }

    @Test
    public void testMondoUdonGetNameMethod() {
        String expected = "Is Gyudon an Udon?";
        assertEquals(mondoUdon.getName(), expected);
    }

    @Test
    public void testMondoUdonGetNoodle() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonGetMeat() {
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonGetTopping() {
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
    }

    @Test
    public void testMondoUdonGetFlavor() {
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }
}
