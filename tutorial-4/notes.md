Pada Lazy Instantiation, instance singleton hanya dibuat saat dibutuhkan saja. Pada Eager Instantiation, instance 
singleton dibuat sejak awal program dijalankan.

Kedua jenis itu memiliki kelebihan dan kekurangannya masing-masing.
Karena pada Lazy Instantiation instance singleton dibuat pada saat dibutuhkan saja, memori tidak diberatkan dengan 
instantiasi objek yang tidak dibutuhkan. Namun pada kasus sebuah singleton instance yang sering digunakan, penggunaan 
Lazy Instantiation justru memperlambat proses karena harus selalu memeriksa apakah instance sudah dibuat atau belum.
Lain halnya jika kita menggunakan Eager Instantiation. Karena instance singleton dibuat sejak awal program, instance
tidak perlu dicek atau dibuat terlebih dahulu karena sudah dibuat sejak awal program dijalankan. Tetapi, akan menjadi
penggunaan memori berlebih jika ternyata instance tersebut tidak digunakan sama sekali.

Analogi mudah yang bisa digunakan adalah saat anda melakukan booting windows ada aplikasi yang dijalankan secara
otomatis. Anda hanya akan buang-buang sumber daya saja jika ternyata aplikasinya tidak kita gunakan. Tentunya kita akan
membiarkan aplikasi yang sering digunakan untuk dijalankan saat booting dan menonaktifkan aplikasi yang jarang digunakan
untuk dijalankan saat proses bootup.