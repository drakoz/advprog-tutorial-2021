package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String attack() {
        return "[insert anime sword skill here]";
    }

    @Override
    public String getType() {
        return "sword";
    }
}
