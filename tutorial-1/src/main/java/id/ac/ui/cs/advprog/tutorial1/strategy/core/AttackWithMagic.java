package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String attack() {
        return "Your light is like the morning sun. Use it to illuminate the darkness, hidden in the ground. " +
                "Praise the Lord, who is now born unto us.";
    }

    @Override
    public String getType() {
        return "magic";
    }
}
