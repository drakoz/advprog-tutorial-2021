package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String defend() {
        return "Placing barrier";
    }

    @Override
    public String getType() {
        return "barrier";
    }
}
