package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        guild.add(this);
    }

    @Override
    public void update() {
        String questType = guild.getQuestType();
        if (questType.equals("D")) {
            this.getQuests().add(guild.getQuest());
        } else if (questType.equals("R")) {
            this.getQuests().add(guild.getQuest());
        }
        System.out.println("Agile: There is a new quest");
    }
}
