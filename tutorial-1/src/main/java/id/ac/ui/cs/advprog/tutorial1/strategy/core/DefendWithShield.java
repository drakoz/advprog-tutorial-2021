package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "A mage defends with shield for some reason";
    }

    @Override
    public String getType() {
        return "shield";
    }
}
