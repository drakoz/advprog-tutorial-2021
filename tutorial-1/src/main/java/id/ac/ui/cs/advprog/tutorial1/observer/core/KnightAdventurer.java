package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        guild.add(this);
    }

    @Override
    public void update() {
//        String questType = guild.getQuestType();
//        if (questType.equals("D")) {
//            this.getQuests().add(guild.getQuest());
//        } else if (questType.equals("R")) {
//            this.getQuests().add(guild.getQuest());
//        } else if (questType.equals("E")) {
//            this.getQuests().add(guild.getQuest());
//        }
        this.getQuests().add(guild.getQuest());
        System.out.println("Knight: There is a new quest");
    }
}
