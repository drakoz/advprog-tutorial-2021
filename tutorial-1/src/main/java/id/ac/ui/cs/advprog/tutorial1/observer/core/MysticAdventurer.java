package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        guild.add(this);
    }

    @Override
    public void update() {
        String questType = guild.getQuestType();
        if (questType.equals("D")) {
            this.getQuests().add(guild.getQuest());
        } else if (questType.equals("E")) {
            this.getQuests().add(guild.getQuest());
        }
        System.out.println("Mystic: There is a new quest");
    }
}
